# установка базового образа. Alpine Linux намного меньше, чем большинство базовых образо
FROM python:3.11-alpine

# Устанавливаем рабочую директорию для проекта в контейнере
WORKDIR /salary_increase

# Копируем содержимое папки в папку в Докере
COPY . .

# скачиваем poetry
RUN pip install poetry

# не создавать виртульную среду
# Скачиваем/обновляем необходимые библиотеки для проекта, исключая установку зависимостей разработчиков
RUN poetry config virtualenvs.create false && poetry install --without dev

# создание переменной среды
ENV PYTHONPATH "${PYTHONPATH}:/salary_increase"

# Устанавливаем рабочую директорию для проекта в контейнере
WORKDIR /salary_increase/salary_increase

# Запуск проекта
CMD ["python", "main.py"]
