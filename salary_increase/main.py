from settings import SECRET_KEY, ALGORITHM, PWD_CONTEXT
import uvicorn as uvicorn
from sqlalchemy.orm import Session
import crud
import models
import schemas
from database import SessionLocal, engine
from typing import Annotated
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


async def get_db():
    """Получение базы данных"""
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


def check_auth(username):
    """Проверка авторизации пользователя"""
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Истек срок действия токена",
        headers={"WWW-Authenticate": "Bearer"},
    )
    if username is None:  # если пользователь неавторизован
        raise credentials_exception
    return credentials_exception


async def get_current_user(username: Annotated[str, Depends(oauth2_scheme)], db: Session = Depends(get_db)):
    """Получение токена с использованием JWT"""
    credentials_exception = check_auth(username)
    try:
        db_user = crud.get_user(db, login=username)
        payload = jwt.decode(db_user.token, SECRET_KEY, algorithms=[ALGORITHM])  # Расшифровка полученного токена
        username: str = payload.get("sub")
        if username is None:  # если пользвоаетля с таким токеном нет
            raise credentials_exception
        token_data = schemas.TokenData(username=username)
    except JWTError:  # Если токен недействителен, то возвращается ошибка
        raise credentials_exception
    user = crud.get_user(db, token_data.username)
    if user is None:
        raise credentials_exception
    return user  # возврат текущего пользователя


def check_admin(username, db):
    """Проверка на статус администратора"""
    print(db)
    print(crud.find_admin(db=db))
    if username != crud.find_admin(db=db).login:
        raise HTTPException(status_code=403, detail="Создавать пользователей может только администратор")


@app.post("/create_admin/", response_model=schemas.User)
def create_admin(user: schemas.UserCreate, db: Session = Depends(get_db)):
    """Создание администратора"""
    if crud.count_user(db) > 0:
        raise HTTPException(status_code=400, detail="Администратор уже существует")

    return crud.create_user(db=db, user=user, role="admin")


@app.post("/create_user/", response_model=schemas.User)
async def create_user(user: schemas.UserCreate, current_user: Annotated[str, Depends(oauth2_scheme)],
                      db: Session = Depends(get_db)):
    """Создание пользователя"""
    # print(oauth2_scheme)
    # print(user)
    # print(current_user)
    print(db)
    check_auth(current_user)
    # print(current_user)
    check_admin(current_user, db)
    db_user = crud.get_user(db, login=user.login)
    if db_user:
        raise HTTPException(status_code=400, detail="Логин уже зарегистрирован")
    return crud.create_user(db=db, user=user, role="user")


@app.post("/new_salary/", response_model=schemas.Work)
async def create_a_salary_increase(work: schemas.WorkCreate,
                                   current_user: Annotated[str, Depends(oauth2_scheme)],
                                   db: Session = Depends(get_db)):
    """Задание текущей зарплаты и даты следующего повышения"""
    check_auth(current_user)
    check_admin(current_user, db)
    try:
        user_id = crud.get_user(db, login=work.login).id
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Неверно введен логин",
        )
    return crud.create_work(db=db, work=work, user_id=user_id)


@app.get("/work")
async def read_work(user: Annotated[schemas.User, Depends(get_current_user)], db: Session = Depends(get_db)):
    """Получение текущей зарплаты и даты следующего повышения"""
    db_user = crud.get_user(db, login=user.login)
    if db_user is None:
        raise HTTPException(status_code=404, detail="Пользователь не найден")
    work = crud.get_work(db, user_id=db_user.id)
    return work


async def verify_password(plain_password, hashed_password):
    """Проверка, соответствует ли полученный пароль сохраненному хэшу"""
    return PWD_CONTEXT.verify(plain_password, hashed_password)


async def authenticate_user(db, username: str, password: str):
    """Аутентификация и возврат пользователя"""
    user = crud.get_user(db, username)
    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


@app.post("/token", response_model=schemas.Token)
async def login(form_data: Annotated[OAuth2PasswordRequestForm, Depends()], db: Session = Depends(get_db)):
    """Авторизация"""
    # возвращает False, если пользователь не существует
    print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz")
    print(form_data, db)
    user = authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Неверное имя пользователя или пароль",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return {"access_token": form_data.username, "token_type": "bearer"}


@app.get("/users/me/", response_model=schemas.User)
async def personal_account(
        current_user: Annotated[schemas.User, Depends(get_current_user)]
):
    """Даннные о пользователе"""
    return current_user


# В разработке продление секретного токена
"""@app.post("/new_token/", response_model=schemas.UserBase)
def secret_token_renewal(current_user: Annotated[str, Depends(oauth2_scheme)], user: schemas.UserBase,
                         db: Session = Depends(get_db)):

    check_auth(current_user)
    check_admin(current_user)

    # crud.update_user(user.login)
    # return create_access_token({"sub": db_user.token})"""

if __name__ == '__main__':
    uvicorn.run("main:app", reload=True)

"""from typing import Annotated

from fastapi import FastAPI, Header, HTTPException
from pydantic import BaseModel

fake_secret_token = "coneofsilence"

fake_db = {
    "foo": {"id": "foo", "title": "Foo", "description": "There goes my hero"},
    "bar": {"id": "bar", "title": "Bar", "description": "The bartenders"},
}

app = FastAPI()


class Item(BaseModel):
    id: str
    title: str
    description: str | None = None


@app.get("/items/{item_id}", response_model=Item)
async def read_main(item_id: str, x_token: Annotated[str, Header()]):
    if x_token != fake_secret_token:
        raise HTTPException(status_code=400, detail="Invalid X-Token header")
    if item_id not in fake_db:
        raise HTTPException(status_code=404, detail="Item not found")
    return fake_db[item_id]


@app.post("/items/", response_model=Item)
async def create_item(item: Item, x_token: Annotated[str, Header()]):
    if x_token != fake_secret_token:
        raise HTTPException(status_code=400, detail="Invalid X-Token header")
    if item.id in fake_db:
        raise HTTPException(status_code=400, detail="Item already exists")
    fake_db[item.id] = item
    return item



"""
