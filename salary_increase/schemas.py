from datetime import date
from pydantic import BaseModel


class UserBase(BaseModel):
    login: str


# создание пользователя
class UserCreate(UserBase):
    password: str


# возврат пользователя
class User(UserBase):
    id: int
    token: str

    class Config:
        orm_mode = True


class WorkBase(BaseModel):
    salary: float
    next_promotion: date


class WorkCreate(WorkBase):
    login: str


class Work(WorkBase):
    user_id: int

    class Config:
        orm_mode = True


# МОдель Pydantic, которая будет использоваться в конечной точке токена для ответа
class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: str | None = None
