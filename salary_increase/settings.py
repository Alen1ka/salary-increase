import json
from pathlib import Path
from passlib.context import CryptContext

f = json.load(open(f'{Path(__file__).resolve().parent}/conf.json'))

NAME_BD = f["name_bd"]

SECRET_KEY = f["secret_key"]

ALGORITHM = f["algorithm"]

ACCESS_TOKEN_EXPIRE_MINUTES = f["access_token_expire_minutes"]  # Истечение срока действия токена

# нужен для кодирования и декодирования пароля
# отказ от всех поддерживаемых схем, за исключением схемы bcrypt по умолчанию
PWD_CONTEXT = CryptContext(schemes=["bcrypt"], deprecated="auto")
