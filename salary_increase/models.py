from sqlalchemy import Column, ForeignKey, Integer, String, Float, Date

from database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    login = Column(String, unique=True, index=True)
    hashed_password = Column(String)
    token = Column(String, unique=True, default=None)
    role = Column(String)


class Work(Base):
    __tablename__ = "work"

    id = Column(Integer, primary_key=True, index=True)
    salary = Column(Float)
    next_promotion = Column(Date)
    user_id = Column(Integer, ForeignKey("users.id"))