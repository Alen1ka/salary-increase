from sqlalchemy.orm import Session
import models
import schemas
from jose import jwt
from settings import SECRET_KEY, ALGORITHM, ACCESS_TOKEN_EXPIRE_MINUTES, PWD_CONTEXT
from datetime import datetime, timedelta


def get_user(db: Session, login: str):
    """Получение пользователя"""
    return db.query(models.User).filter(models.User.login == login).first()


def count_user(db: Session):
    """Посчитать количество пользоавтелей"""
    return db.query(models.User).count()


def find_admin(db: Session):
    """Найти администратора"""
    print(db, models.User.login)
    return db.query(models.User).filter(models.User.role == "admin").first()


def get_work(db: Session, user_id: str):
    """Получение данных о работе"""
    return db.query(models.Work).filter(models.Work.id == user_id).first()


def create_user(db: Session, user: schemas.UserCreate, role: str):
    """Создание пользователя"""
    hashed_password = get_password_hash(user.password)
    # создание токена доступа
    token = create_access_token(data={"sub": user.login})
    db_user = models.User(login=user.login, hashed_password=hashed_password, token=token, role=role)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def create_work(db: Session, work: schemas.WorkCreate, user_id):
    """Создание для пользователя текущей зарплаты и даты следующего повышения"""
    db_work = models.Work(salary=work.salary, next_promotion=work.next_promotion, user_id=user_id)
    db.add(db_work)
    db.commit()
    db.refresh(db_work)
    return db_work


# В разработке
def update_user(db: Session, user: schemas.UserBase):
    """Обновление пользователя"""
    # создание токена доступа
    token = create_access_token(data={"sub": user.login})
    db_user = models.User(login=user.login, token=token)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def create_access_token(data: dict):
    """Генерация нового токена доступа"""
    expires_delta = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)  # указание времени истечения токена
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def get_password_hash(password):
    """Хэширование пароля, поступающего от пользователя"""
    return PWD_CONTEXT.hash(password)
