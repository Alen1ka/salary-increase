## Описание

Данный сервис разработан для просмотра текущей зарплаты сотрудника и даты следующего повышения данной зарплаты. Каждый сотрудник может смотреть только свои данные о зарплате (read_work). 
Администратор имеет право создать данные зарплаты каждого сотрудника (create_a_salary_increase) и создать сотрудника с токеном доступа (create_user), действующим определенный период времени.
До создания пользователя необходимо создать администратора (create_admin).

## Запуск

Клонируем репозиторий:

<code>git clone https://gitlab.com/Alen1ka/salary-increase</code>


Переходим в директорию с проектом:

<code>cd salary-increase/</code>


Устанавливаем зависимости:

<code>poetry install</code>


Запускаем приложение:

<code>python main.py</code>

## Запуск через Docker

Сборка:
<code>docker build -t salary-increase:latest .</code>

Запуск:
<code>docker run -p 5000:8000 salary-increase:latest</code>