from fastapi.testclient import TestClient
from sqlalchemy.orm import sessionmaker
from salary_increase.main import app
from sqlalchemy.orm import Session
from salary_increase.database import Base
from salary_increase.database import engine
from fastapi import Depends, FastAPI
from fastapi.security import OAuth2PasswordBearer

client = TestClient(app)

# models.Base.metadata.create_all(bind=engine)

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

TestingSessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)

"""def override_get_db():
    try:
        db = TestingSessionLocal()
        yield db
    finally:
        db.close()"""


def tests_create_and_auth_user():  # db: Session = Depends(override_get_db)):
    # создание пользователя без регистрации
    response = client.post("/create_user", json={"login": 'artem', "password": 'artem'})
    assert response.status_code == 401
    assert response.json() == {"detail": "Not authenticated"}

    # регистрация
    response = client.post("/token",
                           headers={"accept": 'application/json',
                                    "Content-Type": 'application/x-www-form-urlencoded'},
                           data={"username": 'string', "password": 'string'})
    assert response.status_code == 200
    assert response.json() == {
        "access_token": "string",
        "token_type": "bearer"
    }

    # создание нового пользователя с новым токеном
    response = client.post("/create_user", headers={"accept": 'application/json', "Authorization": "Bearer string",
                                                    "Content-Type": 'application/x-www-form-urlencoded'},
                           json={"login": 'artem2', "password": 'artem2'})
    # print(response.status_code)
    # assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "login": 'artem2',
        "token": "string"
    }

    """

    def test_read_item_bad_token():
        response = client.get("/items/foo", headers={"X-Token": "hailhydra"})
        assert response.status_code == 400
        assert response.json() == {"detail": "Invalid X-Token header"}


    def test_read_inexistent_item():
        response = client.get("/items/baz", headers={"X-Token": "coneofsilence"})
        assert response.status_code == 404
        assert response.json() == {"detail": "Item not found"}


    def test_create_item():
        response = client.post(
            "/items/",
            headers={"X-Token": "coneofsilence"},
            json={"id": "foobar", "title": "Foo Bar", "description": "The Foo Barters"},
        )
        assert response.status_code == 200
        assert response.json() == {
            "id": "foobar",
            "title": "Foo Bar",
            "description": "The Foo Barters",
        }


    def test_create_item_bad_token():
        response = client.post(
            "/items/",
            headers={"X-Token": "hailhydra"},
            json={"id": "bazz", "title": "Bazz", "description": "Drop the bazz"},
        )
        assert response.status_code == 400
        assert response.json() == {"detail": "Invalid X-Token header"}


    def test_create_existing_item():
        response = client.post(
            "/items/",
            headers={"X-Token": "coneofsilence"},
            json={
                "id": "foo",
                "title": "The Foo ID Stealers",
                "description": "There goes my stealer",
            },
        )
        assert response.status_code == 400
        assert response.json() == {"detail": "Item already exists"}
    """
